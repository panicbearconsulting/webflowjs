const mainScript = () => {
    console.log('Emakase Script');

    //Smooth Scroll
    $('html').css('scroll-behavior', 'initial');
    function easing(x) {
        return x === 1 ? 1 : 1 - Math.pow(2, -10 * x)
    }
    const lenis = new Lenis({
        duration: 1.2,
        easing: easing,
        smooth: true,
        direction: 'vertical',
    })

    function raf(time) {
        lenis.raf(time)
        requestAnimationFrame(raf)
    }
    requestAnimationFrame(raf)

    //Scroll Events
    let header = $('.header');
    lenis.on('scroll', function(inst) {
        if (inst.scroll > header.height()) {
            header.addClass('on-scroll');
            if ($(window).width() < 991) {
                $('.header-nav').addClass('on-scroll');
            }
        } else {
            header.removeClass('on-scroll');
            if ($(window).width() < 991) {
                $('.header-nav').removeClass('on-scroll');
            }
        }
    });

    if ($(window).width() <= 991) {
        $('.btn-toggle').on('click', function(e) {
            e.preventDefault();
            $('.header-nav').slideToggle();
            $('.header-nav').toggleClass('active')
            if ($('.header-nav').hasClass('active')) {
                $('body').addClass('on-nav-open')
            } else {
                $('body').removeClass('on-nav-open')
            }
            $('.header-drop-wrap').slideUp();
            $('.header-drop-toggle').removeClass('active');
        })
        $('.header-drop-toggle').on('click', function(e) {
            e.preventDefault();
            $(this).parent('.header-link.mod-mb').next('.header-drop-wrap').slideToggle();
            $(this).toggleClass('active')
        })
    }

    if ($('.ctc-main-form').length) {
        console.log('contact-page');
        //Setup 
        $('#ser-sub-2').val(null);
        $('#ser-sub-2').attr('disabled',true);
        $('#ser-sub-2').select2({
            placeholder: 'Can you be more specific',
        });
        
        // On-change
        $('#ser-sub-1').on('change', function(e) {
            let value = $(this).val();
            if (value == 'Fund-raising Consulting') {
                updateSelect(0);
            } else if (value == 'Management Consulting') {
                updateSelect(1);
            } else if (value == 'Innovation consulting') {
                updateSelect(2);
            } else {
                updateSelect('none');
            }
        })

        function updateSelect(index) {
            if ($('#ser-sub-2').hasClass('select2-hidden-accessible')) {
                $('#ser-sub-2').select2('destroy')
                console.log('destroy')
            }
            
            $('#ser-sub-2 option').remove();
            
            if (index == 0) {
                console.log('first')
                $('#ser-sub-2').append('<option value="Data room preparation">Data room preparation</option>');
                $('#ser-sub-2').append('<option value="Deal structuring">Deal structuring & advisory</option>');
            } else if (index == 1) {
                console.log('second')
                $('#ser-sub-2').append('<option value="Business Management">Business Management</option>');
                $('#ser-sub-2').append('<option value="Financial Management">Financial Management</option>');
                $('#ser-sub-2').append('<option value="People Management">People Management</option>');
                $('#ser-sub-2').append('<option value="Product Development">Product Development</option>');
                $('#ser-sub-2').append('<option value="Marketing">Marketing</option>');
            } else if (index == 2) {
                console.log('third')
                $('#ser-sub-2').append('<option value="Innovation program consultation and implementation">Innovation program consultation and implementation</option>');
                $('#ser-sub-2').append('<option value="Professional competency enhancement programs consultation, building and implementation">Professional competency enhancement programs consultation, building and implementation</option>');
            }

            if (index == 'none') {
                console.log('none')
                $('#ser-sub-2').attr('disabled',true);
            } else {
                console.log('not none')
                $('#ser-sub-2').removeAttr('disabled');
            }
            console.log('re init')
            $('#ser-sub-2').select2({
                placeholder: 'Can you be more specific',
            });
            
        }

        
        //<option value="General enquiries" data-select2-id="select2-data-6-q77s">General enquiries</option>

    }

    if ($('.scjob-hero').length) {
        console.log('job section')        
        // Handle file upload
        function uploadJobform() {
            const currentTextUpload = $('.txt-upload-link').clone().text();
            const maxAllowedSize = 10 * 1024 * 1024;

            $('[data-form="apply"]').on('click', function(e) {
                e.preventDefault();
                let jobTitle = $('.job-hero-title').text();
                $('.job-f-title').text(jobTitle);
                $('#Job-Title').val(jobTitle);
    
                $('body').addClass('on-nav-open')
                $('.overlay-wrap').addClass('active');
            })
    
            $('.form-close-ic').on('click', function(e) {
                e.preventDefault();
                $('body').removeClass('on-nav-open');
                $('.overlay-wrap').removeClass('active');
                $('.job-form-inner').get(0).reset();
                $('.w-form-done').css('display','none');
                $('.job-form-inner').css('display','block');
                resetUploadFile();
            })

            $('.upload-link').on('click', function(e) {
                e.preventDefault();
                $('.input-file').trigger('click');
            })

            $('.input-file').change(function() {
                const localFile = $(this).get(0).files[0];
                console.log(localFile);
                if (localFile) {
                    if (localFile.size >= maxAllowedSize) {
                        alert('Maximum allowed size file is 10MB');
                    } else {
                        $('.ic-upload-link').removeClass('hidden');
                        $('.upload-cancel').addClass('hidden');
                        $('.txt-upload-link').text('Uploading...');
                    
                        uploadFile(localFile, 'career').then((a) => {
                            console.log(a);
                            $('.ph-file-url').val(a.url)
                            doneUploadFile(localFile);
                        })
                    }
                }
            })

            function doneUploadFile(ele) {
                $('.txt-upload-link').text(shortedFileName(ele.name));
                $('.ic-upload-link').addClass('hidden');
                $('.upload-cancel').removeClass('hidden');
            }

            function shortedFileName(name, size = 16) {
                const splitFile = name.split('.');
                function truncate(source, size) {
                    return source.length > size ? source.slice(0, size - 1) + "…" : source;
                }
                return `${truncate(splitFile[0], size)}.${splitFile[1]}`;
            }

            $('.upload-cancel').on('click', function(e) {
                e.preventDefault();
                resetUploadFile();
            })

            function resetUploadFile() {
                $('.ph-file-url').val('');
                $('.txt-upload-link').text(currentTextUpload);
                $('.ic-upload-link').removeClass('hidden');
                $('.upload-cancel').addClass('hidden');
            }
        }
        uploadJobform();

        function uploadFile(file, folder) {
            // https://script.google.com/macros/s/AKfycbzcNcsf-T-rOrgB7VY8ao0B-TD9gQIXn0ceVuDrmc_L7PFHIjT4VeFWz3w2KYFrgFen/exec

            const idScript = 'AKfycbzcNcsf-T-rOrgB7VY8ao0B-TD9gQIXn0ceVuDrmc_L7PFHIjT4VeFWz3w2KYFrgFen'
            const endpoint = `https://script.google.com/macros/s/${idScript}/exec`
            return new Promise((res, rej) => {
                if (!file) res({});
                const reader = new FileReader()
                reader.readAsDataURL(file)
                reader.onload = function (e) {
                    const rawLog = reader.result.split(',')[1];
                    const dataSend = {
                        dataReq: {
                            data: rawLog,
                            name: file.name,
                            type: file.type,
                            folderName: folder
                        },
                        fname: "uploadFilesToGoogleDrive"
                    };
                    fetch(endpoint, { method: "POST", body: JSON.stringify(dataSend) })
                        .then(res => res.json())
                        .then((a) => {
                            res(a)
                        }).catch(e => rej(e))
                }
            })
        }

    }
};

$(document).ready(function() {
    mainScript();
})