const mainScript = () => {
    console.log('Choco Script')
    gsap.registerPlugin(ScrollTrigger);
    
    //Smooth Scroll
    $('html').css('scroll-behavior', 'initial');
    
    function easing(x) {
        return x === 1 ? 1 : 1 - Math.pow(2, -10 * x)
    }

    const lenis = new Lenis({
        duration: 1.2,
        easing: easing,
        smooth: true,
        direction: 'vertical',
    })

    function raf(time) {
        lenis.raf(time)
        requestAnimationFrame(raf)
    }
    requestAnimationFrame(raf)

    function updateInterestRate(wrapper) {
        let bigRate = $('[data-rate-source="big"]').text();
        let smallRate = $('[data-rate-source="small"]').text();
        let otherRate = $('[data-rate-source="other"]').text();
        let amount = $('[data-rate-source="amount"]').text();
        let date = $('[data-rate-source="date"]').text();
        let allRates = $(wrapper).find('[data-rate]');
        allRates.each(function(e) {
            let type = $(this).attr('data-rate');
            if (type == 'big') {
                $(this).text(bigRate)
            } else if (type == 'small') {
                $(this).text(smallRate)
            } else if (type == 'amount') {
                $(this).text(amount)
            } else if (type == 'date') {
                $(this).text(date)
            } else if (type == 'other') {
                $(this).text(otherRate)
            }
        })
    }
    updateInterestRate('.main')
    function createFaq(el,isRichtext = false) {
        if (isRichtext) {
            let ans = '';
            let richTextArray = el.data.content_richtext;
            for (const block of richTextArray) {
                switch (block.type) {
                case 'paragraph':
                        let string = block.text;
                        for (const span of block.spans) {
                            switch (span.type) {
                                case 'strong':
                                string = string.replace(block.text.substring(span.start, span.end),`<strong>${block.text.substring(span.start, span.end)}</strong>`);
                                break;
                                case 'em':
                                string = string.replace(block.text.substring(span.start, span.end),`<em>${block.text.substring(span.start, span.end)}</em>`);
                                break;
                                case 'hyperlink':
                                string = string.replace(block.text.substring(span.start, span.end),`<a href="${span.data.url}" class="span-txt-link hover-un">${block.text.substring(span.start, span.end)}</a>`);    
                                break;
                                case 'label':
                                if (span.data.label == 'big-rate' || span.data.label == 'small-rate' || span.data.label == 'limit-amount' || span.data.label == 'limit-date' || span.data.label == 'other-rate') {
                                    console.log(span.data.label)
                                    let tag;
                                    switch (span.data.label) {
                                        case 'big-rate':
                                        tag = "big"
                                        break;
                                        case 'small-rate':
                                        tag = "small"
                                        break;
                                        case 'other-rate':
                                        tag = "other"
                                        break;
                                        case 'limit-amount':
                                        tag = "amount"
                                        break;
                                        case 'limit-date':
                                        tag = "date"
                                        break;
                                        default:
                                        break;
                                    }
                                    string = string.replace(block.text.substring(span.start, span.end), `<span data-rate=${tag}>${block.text.substring(span.start,span.end)}</span>`)
                                }
                                break;
                                default:
                                break;
                            }
                        }
                        ans += `<p class="txt-16 art-para-sm-sub">${string}</p>`;
                    break;
                case 'image':
                    ans += `<div class="art-img-lg-wrap mod-faq">
                    <img class="img-basic art-main-img" src="${block.url}" alt="${block.alt}" width="${block.dimensions.width}" height="${block.dimensions.height}"/>
                    </div>`;
                    //${block.alt != null ? `<div class="txt-14 art-img-cap">${block.alt}</div>` : "" }
                    break;
                case 'embed':
                    ans += `<div class="art-embed-wrap mod-faq">${block.oembed.html}</div>`
                    break;
                case 'list-item':
                    let listString = block.text;
                        for (const span of block.spans) {
                            switch (span.type) {
                                case 'strong':
                                listString = listString.replace(block.text.substring(span.start, span.end),`<strong>${block.text.substring(span.start, span.end)}</strong>`);
                                break;
                                case 'em':
                                listString = listString.replace(block.text.substring(span.start, span.end),`<em>${block.text.substring(span.start, span.end)}</em>`);
                                break;
                                case 'hyperlink':
                                listString = listString.replace(block.text.substring(span.start, span.end),`<a href="${span.data.url}" class="span-txt-link hover-un">${block.text.substring(span.start, span.end)}</a>`);    
                                break;
                                case 'label':
                                    if (span.data.label == 'big-rate' || span.data.label == 'small-rate' || span.data.label == 'limit-amount' || span.data.label == 'limit-date' || span.data.label == 'other-rate') {
                                        console.log(span.data.label)
                                        let tag;
                                        switch (span.data.label) {
                                            case 'big-rate':
                                            tag = "big"
                                            break;
                                            case 'small-rate':
                                            tag = "small"
                                            break;
                                            case 'other-rate':
                                            tag = "other"
                                            break;
                                            case 'limit-amount':
                                            tag = "amount"
                                            break;
                                            case 'limit-date':
                                            tag = "date"
                                            break;
                                            default:
                                            break;
                                        }
                                        listString = listString.replace(block.text.substring(span.start, span.end), `<span data-rate=${tag}>${block.text.substring(span.start,span.end)}</span>`)
                                    }
                                break;
                                default:
                                break;
                            }
                        }
                    ans += `<li class="txt-16 art-txt-li">${listString}</li>`;
                    break;
                default:
                    console.error(`Unsupported block type: ${block.type}`);
                    break;
                }
            }
            let faqItem = $(`
                <div class="home-faq-item" id="${el.uid}">
                    <a href="#" class="home-faq-item-head w-inline-block">
                        <div class="txt-16 home-faq-item-ques">${el.data.question}</div>
                        <div class="ic-plus-wrap">
                            <div class="ic-plus-inner"></div>
                            <div class="ic-plus-inner mod-rotate"></div>
                        </div>
                    </a>
                    <div class="home-faq-item-body">
                        <div class="txt-16 home-faq--itemans">${ans}</div>
                    </div>
                    <div class="home-faq-bar">
                        <div class="home-faq-bar-inner"></div>
                    </div>
                </div>
            `);
            ans = updateUlLi($(faqItem).find('.home-faq--itemans'))
            function updateUlLi(wraper) {
                const wraperEl = wraper;
                const liEls = wraperEl.find('li');
                liEls.each((i) => {
                    let ulTemplate = $('<ul class="art-txt-ul mod-faq-ans"></ul>')
                    if (liEls.eq(i).prev().get(0) != liEls.eq(i - 1).get(0)) {
                        ulTemplate.clone().insertBefore(liEls.eq(i))
                    }
                })
                liEls.each((i) => {
                    if (liEls.eq(i).prev().prop('tagName') == 'UL') {
                        liEls.eq(i).appendTo(liEls.eq(i).prev())
                    }
                })
            }
            return faqItem;
            // let faqItem = $(`<div class="txt-16">Richtext</div>`)
            // return faqItem;
        } else {
            let faqItem = $(`
                <div class="home-faq-item" id="${el.uid}">
                    <a href="#" class="home-faq-item-head w-inline-block">
                        <div class="txt-16 home-faq-item-ques">${el.data.question}</div>
                        <div class="ic-plus-wrap">
                            <div class="ic-plus-inner"></div>
                            <div class="ic-plus-inner mod-rotate"></div>
                        </div>
                    </a>
                    <div class="home-faq-item-body">
                        <p class="txt-16 home-faq--itemans">${el.data.answer}</p>
                    </div>
                    <div class="home-faq-bar">
                        <div class="home-faq-bar-inner"></div>
                    </div>
                </div>
            `);
            return faqItem
        }
    }
    function animateFaq() {
        $('.home-faq-item-head').on('click', function(e) {
            e.preventDefault();
            if ($(this).hasClass('active')) {
                $('.home-faq-item-head').removeClass('active');
                $('.home-faq-item').removeClass('active');
                $('.home-faq-item-body').slideUp();
            } else {
                $('.home-faq-item-head.active').parent().find('.home-faq-item-body').slideUp();
                $('.home-faq-item-head').removeClass('active');
                $('.home-faq-item').removeClass('active');
                $(this).addClass('active');
                $(this).parent('.home-faq-item').addClass('active');
                $(this).parent().find('.home-faq-item-body').slideDown();
            }
        })
    }

    // Scroll Events
    let header = $('.header');
    function scrollDown() {
        header.addClass('on-hide')
        if ($('.blog-page').length) {
            $('.blog-header').removeClass('on-scroll')
        }
        if ($('.faq-page').length) {
            $('.faq-stick-wrap').removeClass('on-hide')
        }
        if ($(window).width() < 991) {
            if ($('.faq-page').length) {
                $('.faq-toc-inner').removeClass('on-scroll')
            }
        }
        if ($(window).width() < 767) {
            if ($('.term-page').length) {
                $('.term-toc-wrap-inner').removeClass('on-scroll')
            }
        }
    }
    function scrollUp() {
        header.removeClass('on-hide')
        if ($('.blog-page').length) {
            $('.blog-header').addClass('on-scroll')
        }
        if ($('.faq-page').length) {
            $('.faq-stick-wrap').addClass('on-hide')
        }
        if ($(window).width() < 991) {
            if ($('.faq-page').length) {
                $('.faq-toc-inner').addClass('on-scroll')
            }
        }
        if ($(window).width() < 767) {
            if ($('.term-page').length) {
                $('.term-toc-wrap-inner').addClass('on-scroll')
            }
        }
    }
    function isHeaderDarkMode() {
        if ($('.dark-header').length) {
            $('.header').addClass('dark-mode')
            return true;
        }
    }
    isHeaderDarkMode();

    lenis.on('scroll', function (inst) {
        if (inst.scroll > header.height()) {
            header.addClass('on-scroll')
            if (inst.direction == 1) {
                // down
                scrollDown()
            } else if (inst.direction == -1) {
                // up
                scrollUp()
            }
            if ($('.dark-header').length) {
                header.removeClass('dark-mode')
            }
        } else {
            header.removeClass('on-scroll on-hide')
            if ($('.dark-header').length) {
                header.addClass('dark-mode')
            }
        };

        if ($('.faq-page').length) {
            if ($('.faq-stick-wrap').offset().top > $('.sc-faq-hero').height() + 1) {
                $('.faq-stick-wrap').addClass('on-stick')
            } else {
                $('.faq-stick-wrap').removeClass('on-stick')
            }
        }
    });

    const SCRIPT = {};
    SCRIPT.homeScript = () => {
        function homeGetFaq() {
            getAllDataByType('faq').then((res) => {
                if (res) {
                    let allFaq = res;
                    $('.home-faq-main').html('')
                    allFaq.forEach((i) => {
                        if (i.data.show_on_launch_site && i.data.launch_site_home_page) {
                            createFaq(i, true).appendTo($('.home-faq-main'))
                        }
                    });
                    updateInterestRate('.faq-main-wrap');
                    animateFaq();
                }
            });
        }
        homeGetFaq();
    }
    SCRIPT.faqsScript = () => {
        console.log('faq page')
        function faqGetFaq() {
            getAllDataByType('faq_category').then((res) => {
                let allFaqCate = res.sort((cate1, cate2) => cate1.data.order - cate2.data.order);
                updateUICate(allFaqCate)
                updateAllFaq();
            })
        }
        faqGetFaq();
        function updateUICate(allFaqCate) {
            $('.faq-cate-list').html('');
            const faqListTemplate = $('.faq-cate-wrap').eq(0).clone();
            const faqTabTemplate = $('.faq-cate-btn-wrap').eq(0).clone();
            $('.faq-cate-inner').html('')
            $('.faq-main-wrap').html('');
            
            allFaqCate.forEach((faqCate, i) => {
                if (faqCate.data.show_on_launch_site) {
                    //Tab
                    let faqTabHtml = faqTabTemplate.clone();
                    faqTabHtml.find('.faq-cate-btn').text(faqCate.data.name).attr('href',`#${faqCate.uid}`).attr('data-scrollTo', faqCate.uid)
                    $('.faq-cate-inner').append(faqTabHtml)
                    //List
                    let faqListHtml = faqListTemplate.clone().attr('id',`${faqCate.uid}`);
                    faqListHtml.find('.faq-cate-title').text(faqCate.data.name)
                    $('.faq-main-wrap').append(faqListHtml)
                }
            })
            $('.txt-16.home-faq-empty').hide();
        }
        function updateAllFaq() {
            const faqSearchItemTemplate = $('.faq-srch-item').eq(0).clone();
            $('.faq-srch-drop-inner').html('')

            getAllDataByType('faq').then((res) => {
                res.forEach((i) => {
                    if (i.data.show_on_launch_site) {
                        //Faq into their Category
                        let parentSlot = $(`.faq-cate-wrap[id="${i.data.faq_category.uid}"]`).find('.faq-cate-list')
                        createFaq(i,true).appendTo(parentSlot)
                        //Search
                        let faqSearchHtml = faqSearchItemTemplate.clone().attr('data-scrollto',`${i.uid}`)
                        faqSearchHtml.find('.txt-16').text(i.data.question);
                        $('.faq-srch-drop-inner').append(faqSearchHtml)
                    }
                })
                animateFaq();
                updateInterestRate('.faq-main-wrap');
                faqInteraction();
            });
        }
        function faqInteraction() {
            scrollToCategoryOnClick()
            scrollToFaqOnClick();
            searchFaqOnType();
            addFaqIdToURL();
        }
        function addFaqIdToURL() {
            $('.home-faq-item-head').on('click', function(e) {
                e.preventDefault()
                let id = $(this).parent().attr('id');
                let cateId = $(this).closest('.faq-cate-wrap').attr('id')
                console.log(id + cateId)
                const url = new URL(window.location);
                url.searchParams.set('id', id);
                url.searchParams.set('category', cateId);
                history.replaceState({},'', url)
            })
        }
        function scrollToCategoryOnClick() {
            $('.faq-cate-inner .faq-cate-btn').on('click', function(e) {
                e.preventDefault();
                e.stopImmediatePropagation()
                let cateId = $(this).attr('data-scrollTo')
                lenis.scrollTo(`#${cateId}`)
                const url = new URL(window.location);
                url.search = '';
                url.searchParams.set('category', cateId);
                history.replaceState({},'', url)
            })
        }
        function scrollToFaqOnClick() {
            let search = window.location.search.substring(1);
            if (search.includes('=')) {
                let param = JSON.parse('{"' + search.replace(/&/g, '","').replace(/=/g,'":"') + '"}', function(key, value) { return key===""?value:decodeURIComponent(value) })
                if ($(`#${param.id}`).length) {
                    let scrollOffset = $(window).height() * 2 / 10;
                    $(`#${param.id}`).find('.home-faq-item-head').trigger('click')
                    lenis.scrollTo(`#${param.id}`, {offset: -scrollOffset})
                } else if ($(`#${param.category}`).length) {
                    lenis.scrollTo(`#${param.category}`);
                } else {
                    const url = new URL(window.location);
                    console.log(url)
                    url.search = '';
                    history.replaceState({},'', url)
                }
            } else {
                const url = new URL(window.location);
                console.log(url)
                url.search = '';
                history.replaceState({},'', url)
            }
        };
        function searchFaqOnType() {
            let faqs = $('.faq-srch-item');
            let input = $('#faq-search');
            let dropdown = $('.faq-srch-drop-wrap');

            input.on('keyup change', function(e) {
                e.preventDefault();
                let value = $(this).val()
                let compValue = value.toLowerCase().replace(' ','')

                faqs.each((e) => {
                    let ques = faqs.eq(e).find('.txt-16').text()
                    let compQues = ques.toLowerCase().replace(' ','').toLowerCase();
                    if (compQues.includes(compValue)) {
                        faqs.eq(e).removeClass('hidden');
                    } else {
                        faqs.eq(e).addClass('hidden');
                    }

                    //Highlight text
                    let maskedText = new RegExp("(" + value + ")","gi");
                    const newQues = faqs.eq(e).find('.txt-16').text().replace(maskedText, "<span class='hl'>$1</span>")
                    faqs.eq(e).find('.txt-16').html(newQues)
                })

                if (dropdown.find('.faq-srch-drop-inner').height() == 0) {
                    dropdown.find('.faq-srch-empty').slideDown();
                } else {
                    dropdown.find('.faq-srch-empty').slideUp();
                }

                if (input.val() != '') {
                    dropdown.addClass('open');
                } else {
                    dropdown.removeClass('open');
                }
            })
            input.on('focus', function(e) {
                if (input.val() != '') {
                    dropdown.addClass('open');
                }
            })
            input.on('blur', function(e) {
                if (!dropdown.is(':hover')) {
                    dropdown.removeClass('open')
                }
            })
            $('.faq-srch-item').on('click',function(e) {
                e.preventDefault();
                let faqId = $(this).attr('data-scrollto');
                dropdown.removeClass('open')
                let scrollOffset = $(window).height() * 2 / 10;
                $(`#${faqId}`).find('.home-faq-item-head').trigger('click')
                lenis.scrollTo(`#${faqId}`, {offset: -scrollOffset})
            })
        }
        function searchOnSticky() {
            $('.faq-stick-srch').on('click', function(e) {
                e.preventDefault();
                lenis.scrollTo('#faq-search-form', {offset: -40 * unit})
                $('#faq-search-form').find('input').trigger('focus')
            })
        }
        searchOnSticky();
    }

    const pageName = $('.main').attr('data-barba-namespace');
    if (pageName) {
        SCRIPT[(`${pageName}Script`)]();
    }
}

window.onload = mainScript;