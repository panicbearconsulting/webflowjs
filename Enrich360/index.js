gsap.registerPlugin(ScrollTrigger);

const script = () => {
	let widthSC = $(window).width();
	let heightSC = $(window).height();

	// LENIS SCROLL

	$("html").css("scroll-behavior", "initial");

	function easing(x) {
		return x === 1 ? 1 : 1 - Math.pow(2, -10 * x);
	}

	const lenis = new Lenis({
		duration: 1.2,
		easing: easing,
		smooth: true,
		direction: "vertical",
	});

	lenis.on("scroll", ({ scroll, limit, velocity, direction, progress }) => {});

	function raf(time) {
		lenis.raf(time);
		requestAnimationFrame(raf);
	}
	requestAnimationFrame(raf);

	// HOOKS

	const useRem = (initPX, maxWidth) => {
		const calcVW = (initPX / maxWidth) * 100;

		initPX = widthSC < maxWidth ? (calcVW * widthSC) / 1000 : initPX / 10;

		return (value) => value * initPX;
	};

	let rem;
	const responsiveRem = () => {
		rem = useRem(10, 1600);

		switch (true) {
			case widthSC <= 991:
				rem = useRem(10, 991);
				break;
			case widthSC <= 767:
				rem = useRem(15, 767);
				break;
			case widthSC <= 497:
				rem = useRem(10, 497);
				break;
		}
	};
	responsiveRem();

	const hooks = {
		convertQueryURLToObj: (queryString = window.location.search) => {
			try {
				var search = queryString.substring(1);
				return JSON.parse('{"' + decodeURI(search).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g, '":"') + '"}');
			} catch (err) {
				return {};
			}
		},
		changeQueryURL: () => {},
		copyLinkToClipboard: (link) => {
			let $temp = $("<input>");
			$("body").append($temp);
			$temp.val(link).select();
			document.execCommand("copy");
			$temp.remove();
		},
	};

    let lastScrollTop = 0;

	const handleHeader = {
		toggleHide: (e) => {
			let st = $(this).scrollTop();
			if (st > lastScrollTop) {
				if (st > 440) {
					$("header").addClass("hide");
				}
			} else {
				if (st > 440) {
					$("header").addClass("hide");
					$("header").removeClass("hide");
				}
			}
			lastScrollTop = st;
		},
		addBG: () => {
			const scrollPos = {
				x: window.pageXOffset,
				y: window.pageYOffset,
			};
			if (scrollPos.y > $("header").height()) $("header").addClass("active");
			else $("header").removeClass("active");
        },
        toggleNav: () => {
			$(".hamburger").on("click", function (e) {
				e.preventDefault();

                if ($("body").hasClass("openNav")) {
					$("body").removeClass("openNav");

					$(".header-link").removeClass("active");
					$(".nav-dropdown-list").slideUp();
					// $("header").css("transform", "translate(0px, 0px)");

                    lenis.start();
                }
                else {
                    $("body").addClass("openNav");

                    lenis.stop();
                }
            })
		},
		toggleDropdown: () => {
			const DOM = {
				dropdownTitle: $(".header-link"),
				dropdownList: $(".nav-dropdown-list"),
			};

			DOM.dropdownList.hide();
			DOM.dropdownTitle.removeClass('active');

			DOM.dropdownTitle.on("click", function () {
				if ($(this).hasClass(".active")) {
					$(this).removeClass("active");
					$(this).siblings(".nav-dropdown-list").slideUp();
				}
				else {
					$(this).toggleClass("active");
					$(this).siblings(".nav-dropdown-list").slideToggle();

					DOM.dropdownTitle.not($(this)).removeClass("active");
					DOM.dropdownTitle.not($(this)).siblings(".nav-dropdown-list").slideUp();
				}
			});
		},
		resetLinkCurrenPage: () => {
			const DOM = {
				linkPage: $(".nav-dropdown-item-link"),
			}

			DOM.linkPage.on("click", function (e) {
				if (!$(this).attr("href").includes(window.location.pathname)) return;

				$(".hamburger").trigger("click");
			})
		}

	};

	const readMore = (classDetect) => {
		classDetect.hide();

		$(".btn-main.read-more").on("click", function (e) {
			e.preventDefault();
			$(this).hide();
			classDetect.slideDown();
		});
	};

	const parentSelect = (parent) => {
		return (child) => $(parent).find(child);
	}

	const socialShare = (btnList) => {
		let slug = window.location.pathname.split("/").slice(1);

		if (slug.length > 1) {
			const url = window.location.href;
			const title = document.title;

			for (let i = 0; i < $(btnList).length; i++) {
				let href, options;
				const typeBtn = $(btnList).eq(i).attr("data-share-link");

				switch (typeBtn) {
					case "facebook":
						href = "https://www.facebook.com/sharer/sharer.php?u=";
						options = "%3F";
						break;
					case "twitter":
						href = "https://twitter.com/share?url=";
						options = "&summary=";
						break;
					case "linkedin":
						href = "https://www.linkedin.com/shareArticle?mini=true&url=";
						options = "&summary=";
						break;
					default: break;
				}

				// setHref
				$(btnList).eq(i).attr("href", `${href}${url}%2F&title=${title}${options}`);
			}
			$(btnList)
				.closest(".link")
				.on("click", function (e) {
					hooks.copyLinkToClipboard(url);
				});
		}
	}

	const SCRIPT = {};

	SCRIPT.homeScript = () => {
		console.log("homepage 👉️");

		const heroAnim = () => {
			let templateArr = $(".home-hero-circle-arr-wrap").eq(0).clone();
            let countArr = 3;
			let countWave = 4;

			$(".home-hero-circle-gr-arrows").html("");
			for (let x = 0; x < countArr; x++) {
				let html = templateArr.clone();
				html.css("transform", `rotate(${80 + (360 / countArr) * x}deg)`);
				$(".home-hero-circle-gr-arrows").append(html);
			}
			let templateWave = $(".home-circle-wave-item").eq(0).clone();
			$(".home-circle-wave-inner").html("");
			for (let x = 0; x < countWave; x++) {
				let html = templateWave.clone();
				html.css("animation-delay", `${(-24 / countWave) * x}s`);
				$(".home-circle-wave-inner").append(html);
			}
        };

        const swiperCaseStudy = () => {
			const parent = parentSelect(".home-cstudy-area");
            const swiperSetup = new Swiper(parent(".swiper").get(), {
				slidesPerView: 1,
				spaceBetween: rem(30),
				navigation: {
					nextEl: parent(".ctrl-next").get(),
					prevEl: parent(".ctrl-prev").get(),
					disabledClass: "disabled",
				},
				on: {
					slideChange: function () {
						let currentIndex = this.realIndex + 1;
						parent(".current-index").html(currentIndex);
					},
					beforeInit: function () {
						let totalSlide = parent(".swiper-slide").length;
						parent(".total-slide").html(totalSlide);
					},
				},
				breakpoints: {
					768: {
						slidesPerView: 2,
						spaceBetween: rem(24),
					},
				},
			});
		}

		let cateEl = $('[data-link="category"]');
		cateEl.on("click", function (e) {
			$(this).attr("href", `/case-study?category=${$(this).text().replace(" ", "+")}`);
		})

		heroAnim();
		if (widthSC <= 991) {
			swiperCaseStudy();
		}
		if (widthSC <= 767) {
			readMore($(".home-chal-sub-hide"));
		}
    };

    SCRIPT.productScript = () => {
		console.log("productPage 👉️");

		const swiperTimeline = () => {
			const parent = parentSelect(".dehy-hwork-timeline-area-wrap");

            const swiperSetup = new Swiper(parent(".swiper").get(), {
				slidesPerView: 1,
				spaceBetween: 0,
				navigation: {
					nextEl: parent(".ctrl-next").get(),
					prevEl: parent(".ctrl-prev").get(),
					disabledClass: "disabled",
				},
				on: {
					slideChange: function () {
						let currentIndex = this.realIndex + 1;
						parent(".current-index").html(currentIndex);
					},
					beforeInit: function () {
						let totalSlide = parent(".swiper-slide").length;
						parent(".total-slide").html(totalSlide);
					},
				},
			});
        }

		const swiperService = () => {
			const parent = parentSelect(".dehy-service-step");

            const swiperSetup = new Swiper(parent(".swiper").get(), {
				slidesPerView: 1,
				spaceBetween: rem(30),
				navigation: {
					nextEl: parent(".ctrl-next").get(),
					prevEl: parent(".ctrl-prev").get(),
					disabledClass: "disabled",
				},
				on: {
					slideChange: function () {
						let currentIndex = this.realIndex + 1;
						parent(".current-index").html(currentIndex);
					},
					beforeInit: function () {
						let totalSlide = parent(".swiper-slide").length;
						parent(".total-slide").html(totalSlide);
					},
				},
			});
		}

		$(".dehy-prod-list").each(function () {

			const parent = parentSelect($(this));
			let totalIndex = parent(".total-slide").length;

			if (widthSC <= 991) {
				const swiperProduct = new Swiper(parent(".swiper").get(), {
					slidesPerView: 1,
					spaceBetween: rem(30),
					navigation: {
						nextEl: parent(".ctrl-next").get(),
						prevEl: parent(".ctrl-prev").get(),
						disabledClass: "disabled",
					},
					on: {
						slideChange: function () {
							let currentIndex = this.realIndex + 1;
							parent(".current-index").html(currentIndex);
						},
						beforeInit: function () {
							let totalSlide = parent(".swiper-slide").length;
							parent(".total-slide").html(totalSlide);
						},
					},
					breakpoints: {
						768: {
							slidesPerView: 2,
							spaceBetween: rem(20),
						},
					},
				});
			}
		});

		const toggleInterest = () => {
			const DOM = {
				stage: $(".dehy-sc-prod"),
				form: $(".interest-form"),
			};
			gsap.set(".interest-form", { autoAlpha: 0, y: 30 });
			const toggleHide = (autoAlpha) => {
				if (autoAlpha === 1) gsap.to(DOM.form, { ease: "power2", autoAlpha: 1, y: 0, duration: 0.5 });
				else gsap.to(".interest-form", { ease: "power2", autoAlpha: 0, y: 30, duration: 0.5 });
			};
			const tl = gsap.timeline({
				scrollTrigger: {
					trigger: DOM.stage,
					start: "top+=300 bottom",
					end: "bottom+=300 bottom",
					onEnter: () => {
						toggleHide(1);
					},
					onLeave: () => {
						toggleHide(0);
					},
					onEnterBack: () => {
						toggleHide(1);
					},
					onLeaveBack: () => {
						toggleHide(0);
					},
				},
			});
		};

		if (widthSC <= 991) {

        }
        if (widthSC <= 767) {
            swiperTimeline();
			swiperService();
			readMore($(".dehy-hwork-sub").find("p").eq(1));
		}
		toggleInterest();
	}

	SCRIPT.contactScript = () => {
		console.log("contactPage 👉️");

		const cusSelectField = () => {
			$(".cus-select-field").each(function () {
				$(this).children().first().attr("disabled", "disabled");
			});
		};

		cusSelectField();

	}

	SCRIPT.aboutUsScript = () => {
		console.log("aboutPage 👉️");
		const swiperProduct = new Swiper(".about-prod-slide", {
			slidesPerView: 1,
			spaceBetween: 0,
			grabCursor: true,
			loop: true,
			loopAdditionalSlides: 4,
			autoplay: {
				delay: 3000,
			},
			navigation: {
				nextEl: ".about-prod-control-btn.ctrl-next",
				prevEl: ".about-prod-control-btn.ctrl-prev",
				disabledClass: "disabled",
			},
			breakpoints: {
				992: {
					navigation: {
						enabled: false,
					},
				},
			},
		});

		const swiperBenefit = () => {
			const parent = parentSelect(".about-benefit-slide");

			const swiperSetup = new Swiper(parent(".swiper").get(), {
				slidesPerView: 1,
				spaceBetween: rem(30),
				navigation: {
					nextEl: parent(".ctrl-next").get(),
					prevEl: parent(".ctrl-prev").get(),
					disabledClass: "disabled",
				},
				on: {
					slideChange: function () {
						let currentIndex = this.realIndex + 1;
						parent(".current-index").html(currentIndex);
					},
					beforeInit: function () {
						let totalSlide = parent(".swiper-slide").length;
						parent(".total-slide").html(totalSlide);
					}
				}
			});
		}


		const swiperService = () => {
			const parent = parentSelect(".about-service-step");

			const swiperSetup = new Swiper(parent(".swiper").get(), {
				slidesPerView: 1,
				spaceBetween: rem(30),
				navigation: {
					nextEl: parent(".ctrl-next").get(),
					prevEl: parent(".ctrl-prev").get(),
					disabledClass: "disabled",
				},
				on: {
					slideChange: function () {
						let currentIndex = this.realIndex + 1;
						parent(".current-index").html(currentIndex);
					},
					beforeInit: function () {
						let totalSlide = parent(".swiper-slide").length;
						parent(".total-slide").html(totalSlide);
					},
				},
			});
		}

		if (widthSC <= 767) {
			swiperBenefit();
			swiperService();
		}
	}


	SCRIPT.caseStudyScript = () => {
		console.log('case studies');

		let param = location.search;
		if (param != '') {
			$('[fs-cmsfilter-element="reset"]').removeClass('active')
		}

		$('[fs-cmsfilter-element="reset"]').on("click", function (e) {
			$(this).addClass("active");
		});

		$(".case-filter-radio [fs-cmsfilter-field]").on("click", function (e) {
			$('[fs-cmsfilter-element="reset"]').removeClass("active");
		});

		$(".case-item-cate").on("click", function () {
			$(`.case-filter-radio:contains(${$(this).text()})`).trigger("click");
			$('[fs-cmsfilter-element="reset"]').removeClass("active");
		});

		$(".btn-main.mod-news").on("click", function() {
			$(".case-item-cate").on("click", function () {
				$(`.case-filter-radio:contains(${$(this).text()})`).trigger("click");
				const scrollToTop = () => $("html, body").animate({ scrollTop: 0 }, 1000);
				const myTimeout = setTimeout(scrollToTop, 300);

				$('[fs-cmsfilter-element="reset"]').removeClass("active");
			});

			$(".case-item-cate").on("click", function () {
				return false;
			});
		});
	}

	SCRIPT.caseStudyDetailScript = () => {
		console.log('case study detail')
		socialShare(".case-temp-social-ic");

		let cateEl = $('[data-link="category"]');
		cateEl.attr('href',`/case-study?category=${cateEl.text().replace(' ', '+')}`)
	}

	SCRIPT.newsTempScript = () => {
		console.log("newsTemp");

		socialShare(".news-temp-social-ic");

		if (widthSC <= 991) {
			const parent = parentSelect(".news-related-area")
			const swiperSetup = new Swiper(parent(".swiper").get(), {
				slidesPerView: 1,
				spaceBetween: rem(30),
				navigation: {
					nextEl: parent(".ctrl-next").get(),
					prevEl: parent(".ctrl-prev").get(),
					disabledClass: "disabled",
				},
				on: {
					slideChange: function () {
						let currentIndex = this.realIndex + 1;
						parent(".current-index").html(currentIndex);
					},
					beforeInit: function () {
						let totalSlide = parent(".swiper-slide").length;
						parent(".total-slide").html(totalSlide);
					},
				},
				breakpoints: {
					768: {
						slidesPerView: 2,
						spaceBetween: rem(24),
					},
				},
			});
		}
	}

	SCRIPT.cirFoodEcoScript = () => {
		console.log("circleFood 👉️");
		if (widthSC <= 767) {
			const parent = parentSelect(".cir-certi-area");

			const swiperSetup = new Swiper(parent(".swiper").get(), {
				slidesPerView: 1,
				spaceBetween: rem(30),
				navigation: {
					nextEl: parent(".ctrl-next").get(),
					prevEl: parent(".ctrl-prev").get(),
					disabledClass: "disabled",
				},
				on: {
					slideChange: function () {
						let currentIndex = this.realIndex + 1;
						parent(".current-index").html(currentIndex);
					},
					beforeInit: function () {
						let totalSlide = parent(".swiper-slide").length;
						parent(".total-slide").html(totalSlide);
					},
				},
			});
		}
	};

	handleHeader.toggleNav();

	if (widthSC <= 991) {
		handleHeader.toggleDropdown();
		handleHeader.resetLinkCurrenPage();
	}

	$(window).on("scroll", function () {
        handleHeader.addBG();
        handleHeader.toggleHide();
	});

	const pageName = $(".main").attr("name-space");
	if (pageName) {
		SCRIPT[`${pageName}Script`]();
	}
}

window.onload = function () {
    script();
    console.log("loaded");
}